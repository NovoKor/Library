﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ListController : Controller
    {
        private readonly LibraryContext context;

        public IActionResult Index()
        {
            var model = new ListModel { Books = context.Books.ToList() };
            return View(model);
        }

        public IActionResult Create(Book model)
        {
            var method = HttpContext.Request.Method;
            if (method == "POST")
            {
                context.Books.Add(model);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult Edit(Book model)
        {
            var method = HttpContext.Request.Method;
            if (method == "POST")
            {
                Book record = context.Books.First(book => book.Id == model.Id);
                if (record != null)
                {
                    context.Entry(record).CurrentValues.SetValues(model);
                    context.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            model = context.Books.First(book => book.Id == model.Id);
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public IActionResult Delete(Book model)
        {
            context.Books.Remove(model);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ListController(LibraryContext context)
        {
            this.context = context;
        }
    }
}
