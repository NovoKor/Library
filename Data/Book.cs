﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Data
{
    public class Book
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Автор")]
        public string Author { get; set; }
        [Display(Name = "Год")]
        public short Year { get; set; } = (short)DateTime.Now.Year;
    }
}
